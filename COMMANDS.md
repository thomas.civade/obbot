
# All commands

Note: All command should start by the prefix of the bot (Default prefix: !)

### Help Commands
* help basics - List basics commands
* help currency - List currency related commands
* help nsfw - List NSFW commands
* help sfw - List SFW commands
* help admin - List Admin commands
* help extra - List extra commands

### Global
* help - Show the Help Page
* credits - Show some respect
* infos - Show your/room/bot current settings
* infos [@username] - Show user/room/bot current settings
* html [HTML-code] - Echo some html code (Can be used to add color to texts)

### Leveling
* level - Show your current level
* board - Show a board of the users with most XP
* profile color [color] - Set the accent color on your `level` image
* profile tod [option] : Change la banque de phrase à utiliser pour vos "Action ou Vérité" `Options possible: normal, nsfw, alcohol, all`

### Truth Or Dare
* truth - Action ou Vérité: Demander une question Vérité aléatoire!
* dare - Action ou Vérité: Demander une Action aléatoire!
* profile tod [option] - Change la banque de phrase à utiliser pour vos "Action ou Vérité"

**Options possible :** *normal, nsfw, alcohol*

**Note :** *normal : family friendly ; nsfw : mostly naughty stuff ; alcohol : drinking game*

### Currency System
* daily|weekly|monthly - Get your daily/monthly/weekly reward coins
* money - See your balance
* money [@username] - See a username balance
* moneytop | baltop - Show money scoreboard
* money give [@username] [Integer] - Give the value of Integer in money to a user 

### SFW
* cat - Send random picture of cats
* neko - Send random picture of a neko girl
* reddit [subreddit] | r [subreddit] - Use the Reddit API to fetch random post from a specific SFW subreddit (only on allowed subreddit)
* reddit user [user] | r user [user] - Use the Reddit API to fetch random post from a specific reddit user (only on allowed users)
* lemmy [community] | l [community] - Use the Lemmy API to fetch random post from a specific SFW community (only on allowed communities)
* lemmy user [user] | l user [user] - Use the Lemmy API to fetch random post from a specific lemmy user (only on allowed users)
* emote [file-name] - Send an emote (Can be used like custom-emojis on discord, but needs to have a file in the emotes/ folder)
* sauce [N] - Affiche le lien du N ième message en partant de la fin (!reddit et !anime)

### NSFW
* boobs - Boobs!
* ass - Asses!
* pussy - Kittens!
* squirt - Waterpark
* lewd - Lewd anime girls!
* teens - Legal Teens!
* random - Random saved image from the bot library (NSFW & SFW)!
* pornhub [search-terms] - Random video from the Hub using your own search terms!
* anime [search-term] | an [search-term] - Use the Nekos.fun API to search some anime stuff
* reddit [subreddit] | r [subreddit] - Use the Reddit API to fetch random post from a specific NSFW subreddit (only on allowed subreddit)
* reddit user [user] | r user [user] - Use the Reddit API to fetch random post from a specific reddit user (only on allowed users)
* lemmy [community] | l [community] - Use the Lemmy API to fetch random post from a specific NSFW community (only on allowed communities)
* lemmy user [user] | l user [user] - Use the Lemmy API to fetch random post from a specific lemmy user (only on allowed users)

### Admin
* clear [number] - Delete the last N messages
* kick [@user:domain.ext] [raison] - Kick a user
* setting prefix [prefix] - Change the bot prefix
* setting fdpmode [enable/disable] - Enable or not the FDP mode
* setting nsfw [enable/disable] - Enable or not NSFW content in the current room
* setting reddit [nsfw/sfw/user_nsfw/user_sfw] [enable/disable] [subreddit name/username] - Add or remove a subreddit/redditor in the allowed subreddit/redditors list.
* setting lemmy [nsfw/sfw/user_nsfw/user_sfw] [enable/disable] [community name/username] - Add or remove a community/user in the allowed communities/users list.
* setting lang [fr/en] - Change OBbot langage
* setting save [enable/disable] - Enable or not the saving of the generated images (from SFW, NSFW images commands)

