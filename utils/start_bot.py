#!/usr/bin/python
from subprocess import Popen
import sys

filename = "obbot.py"
while True:
    print("\nStarting " + filename)
    p = Popen("python3 " + filename, shell=True)
    p.wait()
    print("\nBot stopped. Restarting now..")
