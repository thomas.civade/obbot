"""
File that contain methods to send content to a matrix room 
like text, image, video or reaction. 
"""
# Import of needed libraries
import os
import magic
import aiofiles
import aiofiles.os
import logging as log
from PIL import Image
from nio import AsyncClient
# Import of OBbot specific stuff
import functions.check as check


""" 
Function to send text messages to a room

async_client : the matrix client
room_id : the room id where the message will be sent
message : the message to sent
formatted_body : Optionnal, permit the format the body of the request
"""
async def sendMessage(async_client : AsyncClient, room_id : str, message : str, formatted_body : str = "") -> None:
    if formatted_body != "":
        content = {
            "body": message,
            "format": "org.matrix.custom.html",
            "formatted_body": formatted_body,
            "msgtype": "m.text",
        }
    else:
        content = {
            "body": message,
            "msgtype": "m.text",
        }
    await async_client.room_send(
        room_id, 
        'm.room.message', 
        content, 
        ignore_unverified_devices=True
    )


"""
Function to send the Typing Indicator (stopped after 10s)

async_client : the matrix client
room_id : the room id where the Typing Indicator will be sent
state : if true start sending the typing indicator, otherwise stop sending it
timeout : time before stop sending the typing indicator 
"""
async def sendTyping(async_client : AsyncClient, room_id : str, state : bool, timeout : int = 10) -> None:
    if(state == True):
        log.info("Sending typing indicator...")
    else:
        log.info("Stopping typing indicator.")
    await async_client.room_typing(room_id, typing_state=state, timeout=timeout)


""" 
Function to send an Image to a room

async_client : the matrix client
room_id : the room id where the image will be sent
image : image filename
"""
async def sendImage(async_client : AsyncClient, room_id : str, image : str) -> None:
    mime_type = magic.from_file(image, mime=True) 
    if await check.checkMimeType(mime_type):
        im = Image.open(image)
        (width, height) = im.size

        file_stat = await aiofiles.os.stat(image)
        async with aiofiles.open(image, "r+b") as f:
            resp, maybe_keys = await async_client.upload(
                f,
                content_type=mime_type, 
                filename=os.path.basename(image),
                filesize=file_stat.st_size)
        content = {
            "body": os.path.basename(image), 
            "msgtype": "m.image",
            "info": {
                "size": file_stat.st_size,
                "mimetype": mime_type,
                "thumbnail_info": None,
                "w": width,
                "h": height,
                "thumbnail_url": None, 
            },
            "url": resp.content_uri,
        }
        await async_client.room_send(room_id, 'm.room.message', content=content, ignore_unverified_devices=True)



""" 
Function to send reactions to an event

async_client : the matrix client
room_id : the room id where the image will be sent
event_id : The event's ID on which the reaction will be posted
emoji : an emote list
"""
async def sendReact(async_client : AsyncClient, room_id : str, event_id : str, emoji) -> None:
    for emote in emoji:
        content = {
            "m.relates_to": {
                "rel_type": "m.annotation",
                "event_id": event_id,
                "key": emote,
            }
        }
        await async_client.room_send(room_id, 'm.reaction', content, ignore_unverified_devices=True)


""" 
Function to send an Video to a room

async_client : the matrix client
room_id : the room id where the image will be sent
file_send : video filename
"""
async def sendVideo(async_client, room_id, file_send):
    mime_type = magic.from_file(file_send, mime=True) 
    file_stat = await aiofiles.os.stat(file_send)
    async with aiofiles.open(file_send, "r+b") as f:
        resp, maybe_keys = await async_client.upload(
            f,
            content_type=mime_type, 
            filename=os.path.basename(file_send),
            filesize=file_stat.st_size)
    content = {
        "msgtype": "m.video",
        "body": os.path.basename(file_send), 
        "url": resp.content_uri,
    }
    await async_client.room_send(room_id, 'm.room.message', content, ignore_unverified_devices=True)


