#!/bin/bash
# On install les dépendences lors du premier démarrage
if [ "$1" ==  "update" ]; then
    echo "Updating pip modules"
    pip freeze > ./bot_storage/requirements-old.txt
    python3 -m venv ./obbot-py-env
    source ./obbot-py-env/bin/activate
    pip3 install -U asyncpraw
    pip3 list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 pip3 install -U
    echo "Python Modules updated to the latest version."
    exit
fi

if [ ! -f ./bot_storage/INSTALLED ]; then
    pip3 install venv
    python3 -m venv ./obbot-py-env
    source ./obbot-py-env/bin/activate
    pip3 install pip -U
    pip3 install -r ./utils/requirements.txt
    touch ./bot_storage/INSTALLED
fi

# Enter the python env.
source ./obbot-py-env/bin/activate

# Start the bot
python3 ./utils/start_bot.py