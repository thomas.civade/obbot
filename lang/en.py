days = "days"
hours = "hours"
minutes = "minutes"
seconds = "seconds"

enableMessage = "enabled"
disableMessage = "disabled"    
NoGoodOptionMessage = "You did not specify a possible option. Available options: <code>lang.option1</code>"
notAllowedToDoThat = "You do not have the permission to execute this action lang.option1."
creditsMessage = "Bot made by @lanseria:matrix.org & @luminosaa:matrix.org\nProfile picture by @sadtusk:matrix.org\nMade using Python and Matrix-NIO\nUtilises les API nekos.life et Reddit pour les images"
levelUPMessage = "event.sender just leveled up ! Now at level lang.option1"
setColorMessage = "Your color has been changed to: <font color='lang.option1'><code>lang.option1</code></font>"
wrongColorMessage = "Your color is not in the right format, please use this format: <code>#RRGGBB</code>"
mustSupplyAColor = "Vous n'avez pas spécifié de couleur. Merci d'en spécifier une au format: <code>#RRVVBB</code>"
setPrefixMessage = "Bot prefix changed for: lang.option1"
setFDPMessage = "FDP Mode has been lang.option1"
setSavingMessage = "Saving feature has been lang.option1"
setNSFWMessage = "NSFW mode has been lang.option1 on this room"
setLangMessage = "Language has been set to lang.option1"
nothingFoundTerms = "No image found for: <code>lang.option1</code>"
NSFWDisable = "NSFW content is not enable in this room."
alreadyNSFWRoom = "NSFW is already enabled on this room."
AnimeNotInList = "Vous n'avez pas spécifié un terme autorisé. Liste des choix possible en tapant <code>lang.option1help sfw</code> ou <code>lang.option1help nsfw</code>."
userDoesntExist = "L'utilisateur demandé n'existe point."

# Reddit
SubNotInList = "Vous n'avez pas spécifié un subreddit ou un redditeur autorisé. Liste des choix possible en tapant <code>lang.option1help sfw</code> ou <code>lang.option1help nsfw</code>."
redditGotError = "Il y a eu un problème avec Reddit, merci de réessayer. Erreur: "
# Subreddit settings
subredditAddedToTheList = "This subreddit has been added to allowed list."
subredditRemovedToTheList = "This subreddit has been removed to allowed list."
subredditAlreadyOnTheList = "This subreddit is already on the allowed list."
# Redditors settings
redditUserAddedToTheList = "This redditor has been added to allowed list."
redditUserRemovedToTheList = "This redditor has been removed to allowed list."
redditUserAlreadyOnTheList = "This redditor is already on the allowed list."

# Lemmy
LemmyCommunityNotInList = "Vous n'avez pas spécifié une communauté ou un utilisater autorisé. Liste des choix possible en tapant <code>lang.option1help sfw</code> ou <code>lang.option1help nsfw</code>."
lemmyGotError = "Il y a eu un problème avec Lemmy, merci de réessayer. Erreur: "
# Communities settings
LemmyCommunityAddedToTheList = "This community has been added to allowed list."
LemmyCommunityRemovedToTheList = "This community has been removed to allowed list."
LemmyCommunityAlreadyOnTheList = "This community is already on the allowed list."
# Users settings
LemmyUserAddedToTheList = "This user has been added to allowed list."
LemmyUserRemovedToTheList = "This user has been removed to allowed list."
LemmyUserAlreadyOnTheList = "This user is already on the allowed list."

notAllowedToRedact = "Je n'ai point les droits de supprimer des messages dans ce channel."
PostedBy = "Posté par"

noSauceHere = "There is no sauce yet in this room."
notAInt = "You didn't specify a correct number."
sauceNotNull = "Error: on ne peut pas selectionner une images négative ou null"
notEnoughSauce = "Il n'y a pas autant de sauces dans ce channel, max: lang.option1"
CMDUsed = "Command used to obtain : "

currencyMoneyOwned = "lang.option1, vous avez <code>lang.option2</code> lang.option3"
currencyUserMoney = "lang.option1 a <code>lang.option2</code> lang.option3"

currencyDailyGiven = "Hey lang.option3 ! Vous avez reçu <code>lang.option1</code> lang.option2. Revenez dans 24 heures pour une autre récompense!"
currencyDailyAlreadyClaimed = "Vous avez déjà reçu votre récompense journalière. Revenez dans lang.option1"
currencyWeeklyGiven = "Hey lang.option3 ! Vous avez reçu <code>lang.option1</code> lang.option2. Revenez dans 7 jours pour une autre récompense!"
currencyWeeklyAlreadyClaimed = "Vous avez déjà reçu votre récompense hebdomadaire. Revenez dans lang.option1"
currencyMonthlyGiven = "Hey lang.option3 ! Vous avez reçu <code>lang.option1</code> lang.option2. Revenez dans 1 mois pour une autre récompense!"
currencyMonthlyAlreadyClaimed = "Vous avez déjà reçu votre récompense mensuel. Revenez dans lang.option1"

currencyTradeSyntax = "lang.option1, vous n'avez pas utilisé la bonne syntax: <code>lang.option2money give @User [Nombre Entier]</code>"
currencyTradeNotEnoughMoney = "lang.option1, vous n'avez pas assez d'argent pour effectuer ce virement."
currencyTradeGaveMoney = "lang.option1 a donné lang.option2 lang.option3 à lang.option4"
currencyTradeCantGiveToSelf = "lang.option1, vous ne pouvez pas vous envoyer de l'argent à vous même."

TODWrongsettingToChange = "Vous n'avez pas spécifié un paramètre correcte. Options disponible: <code><i>normal, nsfw, alcohol</i></code>"
TODsettingChanged = "Vous avez changé votre banque de questions action ou vérité pour: "

infosListToD = "Liste action ou vérité"
infosNumberOf = "Nombre de"
infosColor = "Couleur"
infosSavingFeature = "Saving Feature"
infosFDPMode = "FDP Mode"
infosPrefix = "Prefix"
infosCurrencyName = "Currency Name"
infosNSFW = "NSFW"
infosRoomOptions = "Room Options"
infosUserOptions = "User Options"
infosYourOptions = "Your Options"
infosBotOptions = "Bot Options"

def helpFonction(prefix, page):
    help_default = '''Outbreaker Bot | Documentation
    <br><code><b>''' + prefix + '''help basics</b></code> : List basics commands
    <br><code><b>''' + prefix + '''help currency</b></code> : List currency related commands
    <br><code><b>''' + prefix + '''help nsfw</b></code> : List NSFW commands
    <br><code><b>''' + prefix + '''help sfw</b></code> : List SFW commands
    <br><code><b>''' + prefix + '''help admin</b></code> : List Admin commands
    <br><code><b>''' + prefix + '''help extra</b></code> : List extra commands'''

    help_basics = '''Outbreaker Bot | Documentation | Basics
    <br><code><b>''' + prefix + '''level</b></code> : Affiche votre niveau d'experience
    <br><code><b>''' + prefix + '''level [@username]</b></code> : Affiche le niveau d'experience d'un utilisateur
    <br><code><b>''' + prefix + '''infos</b></code> : Affiche vos informations
    <br><code><b>''' + prefix + '''infos [@username]</b></code> : Affiche les informations d'un utilisateur
    <br><code><b>''' + prefix + '''board</b></code> : Redirige vers le leaderboard du bot
    <br><code><b>''' + prefix + '''profile color [color]</b></code> : Change la couleur de votre carte de "level"
    <br><b>Note : </b> <em>Seules les couleurs au format HTML sont acceptée (Exemple: #111111, #04aa6d, ...)</em>
    <br><code><b>''' + prefix + '''profile tod [option]</b></code> : Change la banque de phrase à utiliser pour vos "Action ou Vérité"
    <br><b>Options possible: </b> <em>normal, nsfw, alcohol</em>
    <br><b>Note : </b> <em>normal : family friendly ; nsfw : mostly naughty stuff ; alcohol : drinking game</em>
    '''

    help_currency = '''Outbreaker Bot | Documentation | Currency System
    <br><code><b>''' + prefix + '''daily</b></code> : Get your daily reward coins [1 - 25]
    <br><code><b>''' + prefix + '''weekly</b></code> : Get your weekly reward coins [50 - 100]
    <br><code><b>''' + prefix + '''monthly</b></code> : Get your monthly reward coins [200 - 1000]
    <br><code><b>''' + prefix + '''money</b></code> : Show your balance
    <br><code><b>''' + prefix + '''money [@username]</b></code> : Show a user balance
    <br><code><b>''' + prefix + '''moneytop</b></code> : Show money scoreboard
    <br><code><b>''' + prefix + '''money give [@username] [Integer]</b></code> : Give the value of Integer in money to a user '''

    help_nsfw = '''Outbreaker Bot | Documentation | NSFW
    <br><code><b>''' + prefix + '''lemmy user | ''' + prefix + '''l user [lemmy_user]</b></code> : Envoyer une image aléatoire d'un utilisateur Lemmy
    <br><b>Allowed users : </b> <em>lang.userlemmynsfw</em>
    <br><code><b>''' + prefix + '''lemmy | ''' + prefix + '''l [community]</b></code> : Envoyer une image aléatoire d'une communauté Lemmy
    <br><b>Allowed communities : </b> <em>lang.communitieslemmynsfw</em>
    <br><code><b>''' + prefix + '''reddit user | ''' + prefix + '''r user [subreddit]</b></code> : Envoyer une image aléatoire d'un redditeur
    <br><b>Allowed users : </b> <em>lang.userredditnsfw</em>
    <br><code><b>''' + prefix + '''reddit | ''' + prefix + '''r [subreddit]</b></code> : Envoyer une image aléatoire d'un subreddit
    <br><b>Allowed subreddit : </b> <em>lang.subredditnsfw</em>
    <br><code><b>''' + prefix + '''anime | ''' + prefix + '''an [option]</b></code> : Rechercher des images NSFW d'anime
    <br><b>Allowed options : </b> <em>lang.animesearchnsfw</em>
    <br><code><b>''' + prefix + '''boobs</b></code> : Des seins!
    <br><code><b>''' + prefix + '''ass</b></code> : Des culs!
    <br><code><b>''' + prefix + '''pussy</b></code> : Des chatons!
    <br><code><b>''' + prefix + '''squirt</b></code> : Un peu d'eau!
    <br><code><b>''' + prefix + '''lewd</b></code> : Lewd anime girls!
    <br><code><b>''' + prefix + '''teens</b></code> : Adolescente légale!
    <br><code><b>''' + prefix + '''random</b></code> : Random saved image from the bot library (NSFW & SFW)!
    <br><code><b>''' + prefix + '''pornhub [search-terms]</b></code> : Une video du Hub aléatoire selon vos termes de recherche.'''

    help_sfw = '''Outbreaker Bot | Documentation | SFW
    <br><code><b>''' + prefix + '''lemmy user | ''' + prefix + '''l user [lemmy_user]</b></code> : Envoyer une image aléatoire d'un utilisateur Lemmy
    <br><b>Allowed users : </b> <em>lang.userlemmysfw</em>
    <br><code><b>''' + prefix + '''lemmy | ''' + prefix + '''l [community]</b></code> : Envoyer une image aléatoire d'une communauté Lemmy
    <br><b>Allowed communities : </b> <em>lang.communitieslemmysfw</em>
    <br><code><b>''' + prefix + '''reddit user | ''' + prefix + '''r user [subreddit]</b></code> : Envoyer une image aléatoire d'un redditeur
    <br><b>Allowed users : </b> <em>lang.userredditsfw</em>
    <br><code><b>''' + prefix + '''reddit | ''' + prefix + '''r [subreddit]</b></code> : Use the Reddit API to fetch random post from a specific SFW subreddit (only on allowed subreddit)
    <br><b>Allowed subreddit : </b> <em>lang.subredditsfw</em>
    <br><code><b>''' + prefix + '''anime | ''' + prefix + '''an [option]</b></code> : Rechercher des images d'anime
    <br><b>Allowed options : </b> <em>lang.animesearchsfw</em>
    <br><code><b>''' + prefix + '''greentext</b></code> : Les textes verts de l'internet
    <br><code><b>''' + prefix + '''sauce [N]</b></code> : Affiche le lien du N ième message en partant de la fin (!reddit et !anime)
    <br><code><b>''' + prefix + '''emote [filename]</b></code> : Send an emote
    <br><b>Possible filename : </b> <em>lang.emotes</em>
    <br><code><b>''' + prefix + '''cat</b></code> : Send random picture of cats!
    <br><code><b>''' + prefix + '''truth</b></code> : Action ou Vérité: Demander une question Vérité aléatoire!
    <br><code><b>''' + prefix + '''dare</b></code> : Action ou Vérité: Demander une Action aléatoire!
    <br><code><b>''' + prefix + '''neko</b></code> : Send random picture of a neko girl'''

    help_extra = '''Outbreaker Bot | Documentation | Extras
    <br><code><b>''' + prefix + '''ping</b></code> : Pong!
    <br><code><b>''' + prefix + '''html</b></code> : Dis au bot d'afficher du contenu formatté en HTML!
    <br><code><b>''' + prefix + '''credits</b></code> : Qui sont les boss ?'''

    help_admin = '''Outbreaker Bot | Documentation | Admin Zone
    <br><code><b>''' + prefix + '''clear [nombre]</b></code> : Supprimer les N derniers messages
    <br><code><b>''' + prefix + '''kick [@user:domain.ext] [raison]</b></code> : Kicker un utilisateur
    <br><code><b>''' + prefix + '''setting prefix [prefix]</b></code> : Changer le prefix du bot
    <br><code><b>''' + prefix + '''setting fdpmode [enable/disable]</b></code> : Activer ou non le FDPMODE
    <br><code><b>''' + prefix + '''setting nsfw [enable/disable]</b></code> : Activer ou non le contenu NSFW  
    <br><code><b>''' + prefix + '''setting reddit [nsfw/sfw/user_nsfw/user_sfw] [enable/disable] [subreddit_name/username]</b></code> : Enable or disable a subreddit/redditor in the allowed subreddits/redditors list.
    <br><code><b>''' + prefix + '''setting lemmy [nsfw/sfw/user_nsfw/user_sfw] [enable/disable] [community_name/username]</b></code> : Enable or disable a community/user in the allowed communities/users list.
    <br><code><b>''' + prefix + '''setting lang [fr/en]</b></code> : Changer la langue du bot
    <br><code><b>''' + prefix + '''setting save [enable/disable]</b></code> : Activer ou non la sauvegarde des photos et des liens'''

    if page == "basics":
        return help_basics
    if page == "currency":
        return help_currency
    elif page == "nsfw":
        return help_nsfw
    elif page == "sfw":
        return help_sfw
    elif page == "extra":
        return help_extra
    elif page == "admin":
        return help_admin
    else:
        return help_default

def todTruthFonction(setting):
    normal =  ["a", "b"]
    nsfw =  ["c", "d"]
    alcohol =  ["e", "f"]

    if setting == "nsfw":
        return nsfw
    elif setting == "alcohol":
        return alcohol
    else:
        return normal

def todDareFonction(setting):
    normal =  ["a", "b"]
    nsfw =  ["c", "d"]
    alcohol =  ["e", "f"]

    if setting == "nsfw":
        return nsfw
    elif setting == "alcohol":
        return alcohol
    else:
        return normal

fdp_insults = ["fdp", "salope", "ntm", "nto", "enfoiré", "nique ta", "nique ton", "fils de", "enfoire", 
"nwar", "con", "pd", "pute", "blc", "de tes morts", "enculé", "ta gueule", "tg", "connard", "résidu de capote", "nigga"] 
# Phrases de réponses 
fdp_phrases = ["C'est pas cool ça ! :(", "Arrête d'être méchant ! :(", "Pourquoi tant de haine ? :c", 
"Je suis outré.e ! :{", "Je vais dire à ma maman ! :'(", "Bon fils de pute tu vas te calmer ?", 
"Je vais dire ta maman ! :'(", " je vais dire à la maîtresse ! :'(", "What are you doing step bro ?", 
"Tu vas aller en enfer pour ça ! >:[", "c'est pas bien de dire des gros mots !", "Langage svp -_-", 
"T'es dégueulasse !", "T'es serieux d'insulter comme ça ??"]
fdp_phrasesMentions = ["Qu'est-ce que tu me baragouine sale fils de chien ?", "ui ?", "tu veux quoi ?", 
"ME MENTIONNE PAS BORDEL", "Je sais, t'as peut être besoin de contact humain, mais je suis un putain de bot enculé va.", 
".", "Laisse moi tranquille.", "Ouais, Lumi est un connard je confirme.", "J'ai mieux à faire que de causer avec toi", 
"Demain après les cours t mor", "Blablabla- oui ? Tu veux quelque chose ?", "Reste associable enflure.",
"J'baise ta mère fils de pute, ton père, ta grand-mère, ta femme j'la fait péta-tapiner dans le 92 avec les kheys p'tit con !", 
"En vrai, tu crois pas que je m'en branle de ce que tu me raconte ?", "T'assieds pas à ma table enfoiré", 
"Purée il est d'un d'ces bleu le ciel aujourd'hui !", "Att je finis un truc j'te répond après", "Je sais pas, me demande pas mon avis"] 
fdp_excuses = ["désolé", "my bad", "pardon", "j'm'excuse", "oui maitre", "sorry"]
fdp_edited = ["Bah alors, event.sender on assume pas, on edite ses messages ? Je vois tout tu sais Je suis omniscient.", "Alors event.sender, on fais des fautes, on edite ses messages?"]

# The bot will react with those when someone write "rip"
fdp_ripEmote = ["😔", "🙏", "🪦"]