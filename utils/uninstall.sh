#!/bin/bash
# This file uninstall the service file needed to autostart the bot at boot
# 
# Made by Krafting - https://www.krafting.net

# On stoppe le service si besoin
sudo systemctl stop obbot
sudo systemctl disable obbot

# On supprime les fichiers
sudo rm -rf /opt/obbot/
sudo rm /lib/systemd/system/obbot.service

## On retire l'utilisateur
sudo userdel -r obbot

echo ""
echo ""
echo "Le bot a été correctement déinstallé."
echo ""
echo ""