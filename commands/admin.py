# Import of needed libraries
import os
import json
import random
import logging as log
import asyncio
from urllib.request import urlopen

# Import of OBbot specific stuff
import functions.bases as bases
import functions.download as download
import functions.messages as messages
import functions.check as check


async def kickUser(async_client, room_id, event, lang):
    userRequested = await check.checkMention(event)
    if userRequested:
        if len(event.body.split()) >= 2:
            reasonForKick = event.body.split(' ', 2)[2]
        else:
            reasonForKick = "Aucune raison spécifiée. (change to lang plz)"
        x = await async_client.room_kick(room_id, user_id=userRequested, reason=reasonForKick)
        if str(x).startswith("RoomKickError"):
            log.info('Kick error (Not in room, not enough permission')
            log.info(x)
        else:
            log.info(event.sender + " a kické" + userRequested + " parce que: " + reasonForKick)
    else: 
        log.info('You need to specify someone.. (change to lang..)')
    return

async def clearMessage(async_client, room_id, event, lang, prefix):
    num_msg = event.body.lower().replace(prefix + "clear", "", 1).strip()
    if num_msg.isnumeric():
        log.info("Yup, c'est un nombre: " + num_msg)
    else:
        log.info("Nope, Non : " + num_msg)
        return
    fetchLastNMessage = await async_client.room_messages(room_id=room_id, start=async_client.next_batch, limit=num_msg)

    for eventDel in fetchLastNMessage.chunk:
        if "body" in eventDel.source['content']:
            log.info(eventDel.source['content']['body'] + " ||| " + eventDel.source['event_id'])
            deleteMessage = await async_client.room_redact(room_id, event_id=eventDel.source['event_id'], reason="Deleted by OBbot")
            if str(deleteMessage).startswith('RoomRedactError: M_FORBIDDEN'):
                log.info("The bot don't have permission to redact events in the room" + str(room_id) + " : " + str(deleteMessage))
                await messages.sendMessage(async_client, room_id, lang.notAllowedToRedact)
                break
            if str(deleteMessage).startswith("RoomRedactError: M_LIMIT_EXCEEDED"):
                log.info(deleteMessage)
                await asyncio.sleep(5)
                deleteMessage = await async_client.room_redact(room_id, event_id=eventDel.source['event_id'], reason="Deleted by OBbot")
            await asyncio.sleep(0.25)
        else:
            log.info("Pas un message écrit, on y touche pas.")