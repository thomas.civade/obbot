# Import of needed libraries
import os
import random
import pornhub
import logging as log
from rule34Py import rule34Py

# Import of OBbot specific stuff
import functions.messages as messages
import functions.bases as bases
import functions.download as download


# Search Rule34 and send images or videos using keywords
async def sendR34Image(async_client, room_id, event, config, prefix, savingFeature):
    # Si le channel n'est pas dans la NSFW room list
    if room_id not in config['nsfw_rooms']:
        await messages.sendMessage(async_client, room_id, '', lang.NSFWDisable)
        return

    await messages.sendTyping(async_client, room_id, True)
    search = event.body.lower().replace(prefix + "rule34", "", 1).strip()
    r34Py = rule34Py()
    try:
        rand_num = random.randint(0, len(r34Py.search(search.split(), 0, 100))-1)
        searchImage = r34Py.search(search.split(), 0, 100)
        imageURL = (searchImage[rand_num].image).replace('api-cdn-us', 'api-cdn', 1)
    except Exception:
        # log.info(traceback.format_exc())
        log.info("rule34 image Not found")
        await messages.sendMessage(async_client, room_id, '', lang.nothingFoundTerms.replace("lang.option1", search))
        return
    file_extension = os.path.splitext(imageURL)[1]
    if file_extension in config['allowedImages']:
        image = await download.downloadFile(savingFeature, imageURL, './image', './library/rule34/', 'R34_' + search + "_")
        await messages.sendImage(async_client, room_id, image)
    else:
        await messages.sendMessage(async_client, room_id, imageURL)
        if savingFeature:
            await bases.addLineToFile("./library/rule34/links", imageURL + " - " + search)
    await messages.sendTyping(async_client, room_id, False)
    await bases.saveSauce(room_id, imageURL, "!rule34 " + search, config['storage_path'])


# Search pornhub and send links using keywords
async def sendPornHubLink(async_client, room_id, event, config, prefix, savingFeature):
    searchTerms = event.body.lower().replace(prefix + "pornhub", "", 1).strip().split()
    if searchTerms == "":
        keywords_table = ["", "teen", "teens", "boobs","amateur", "blonde", "step", "lesbian", "threesome", "creampie"]
        searchTerms = random.sample(keywords_table, 2)
    # Si le channel n'est pas dans la NSFW room list
    if room_id not in config['nsfw_rooms']:
        await messages.sendMessage(async_client, room_id, '', lang.NSFWDisable)
        return
    await messages.sendTyping(async_client, room_id, True)
    video_page = random.randint(1,10)
    all_submits_PH = []
    try:
        client = pornhub.PornHub(searchTerms)
        for video in client.getVideos(50, page=video_page):
            all_submits_PH.append([video["url"], video["name"]])
        videoInfos = random.choice(all_submits_PH)
        await messages.sendMessage(async_client, room_id, videoInfos[1] + "\n\n" +videoInfos[0])
        if savingFeature:
            await bases.addLineToFile("./library/.pornhub.list", videoInfos[1] + " - - " + videoInfos[0])
    except Exception:
        log.info('An error occured using the pornhub command.')
        await messages.sendMessage(async_client, room_id, "Error with pH (message to edit later)")
        return
    await messages.sendTyping(async_client, room_id, False)


# Random image from the bot library
async def sendRandomFileFromLibrary(async_client, room_id, event, config):
    # Si le channel n'est pas dans la NSFW room list
    if room_id not in config['nsfw_rooms']:
        await messages.sendMessage(async_client, room_id, '', lang.NSFWDisable)
        return
    await messages.sendTyping(async_client, room_id, True)
    # On check si c'est pas infichier caché (style .gitkeep)
    imageFolderRandom = "."
    while imageFolderRandom.startswith('.'):
        imageFolderRandom = random.choice(os.listdir("./library/"))
    imageFolder = './library/' + imageFolderRandom
    if imageFolderRandom == "anime":
        imageFolderRandomAnime = random.choice(os.listdir("./library/anime/"))
        imageFolder = './library/anime/' + imageFolderRandomAnime
    elif imageFolderRandom == "users":
        imageFolderRandomAnime = random.choice(os.listdir("./library/users/"))
        imageFolder = './library/users/' + imageFolderRandomAnime
    image = random.choice(os.listdir(imageFolder))
    image = imageFolder + '/'+ image
    file_extension = os.path.splitext(image)[1]
    if file_extension in config['allowedImages']:
        await messages.sendImage(async_client, room_id, image)
        await messages.sendMessage(async_client, room_id, "Random Category: " + imageFolderRandom)
    elif file_extension in config['allowedVideos']:
        await messages.sendVideo(async_client, room_id, image)
        await messages.sendMessage(async_client, room_id, "Random Category: " + imageFolderRandom)
    else:
        log.info(image)
        log.info("Error with random, couldn't upload file.")
    await messages.sendTyping(async_client, room_id, False)