# What each and every file does.

## Folders:


+ bot_storage : Used for any specific bot storage, database and history.
+ bot_storage_temp : Same as bot_storage but with temporary purpose; can be deleted without any consequences
+ fonts : Any font used by the bot when generating images
+ emotes : Custome emotes as images used by the bot with the !emote command.
+ docs : Extra documentation for the bot
+ lang : Language files for translating the bot
+ library : All the bot library if the SavingFeature is enabled in setting. (Where the bot download thing from lemmy, reddit and more)
+ obbot-py-env : Python custom environnement for the bot. Where dependencies is installed so it doesn't mess with the host system.
+ utils : Some utilities for the bot, such as starting script or images mask. These files are required.
+ commands : Files to manage commands of the bots
+ functions : Files with function used in most of the commands.

## commands folder

Commands of the bot.

+ admin.py : Admin-only commands
+ anime.py : Handling the !anime command.
+ fdp_mode.py : When the bot doesn't likes you.
+ money.py : Handling the !money commands and monetary system.
+ normal.py : Some "normal" commands which doesn't fit other categories
+ nsfw.py : Handling the nsfw commands
+ reddit.py : Handling the !reddit command, both NSFW and SFW
+ lemmy.py : Handling the !lemmy command, both NSFW and SFW
+ settings.py : Change settings of users and the bot
+ truthordare.py : The Truth or Dare built-in game
+ xp.py : Manage XP given to users for being active on the room.

## functions folder

Bases functions used throughout the bot.

+ bases.py : Basic functions used in most places.
+ check.py : Functions to check for stuff in stuff.
+ download.py : Functions to download most files from the internet with a given URL
+ messages.py : Functions to send messages and others event with the bot.

## Files

+ COMMANDS.md : All the bot's commands
+ config.json : Config of the bot, is not present in the git repo, you need to rename config.template.json and update the file to contain your bot infos.
+ config.template.json : Base template for the configuration of the bot.
+ README.md : Basic informations about the bot and how to use it.
+ obbot.py : The bot, handling event and sending them to the right functions.

## utils folder 

Utilities to manage the bot.

+ avatarMask.jpg : Used to make rounded avatar in the profile picture when using the !level command
+ install.sh : Install the bot on Linux
+ obbot.service : Service to auto-start the bot on Linux
+ requirements.txt : Python pip requirements
+ start_bot.py : Start the bot
+ start_bot.sh : Start the bot and auto-restart when it crashes
+ uninstall.sh : Uninstall the bot from your system
+ update.sh : Update the bot to the latest version
+ verif.py : DEPRECATED: Old script to verify the session of the bot. Not used anymore.