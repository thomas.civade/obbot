"""
File that contain method to download and store content.
"""

# Import of needed libraries
import os
import time
import random
import aiohttp
import requests
import datetime
import aiofiles
import logging as log
import yt_dlp as youtube_dl

# Import of OBbot specific stuff
import functions.messages as messages
import functions.check as check

"""
A method to download a file that return the path where the file is now stored

savingFeature : if true, save the file in savng_path
url : file url
default_path : if the file isn't store, the file will be temporary store add this path
filename  : the downloaded filename
exten : file extension for storage
dateFileName : if true : add the date in the file name 
ytdlp : if true, force to use YoutubeDL library. 

"""
async def downloadFile(savingFeature : bool, url : str, default_path : str, saving_path : str = "./library/", filename : str = "", exten : str = "", dateFileName : bool = True, ytdlp : bool = False) -> str:
    currentDate = datetime.datetime.now().strftime("%d-%m-%Y_%X")
    randomInt = random.randint(1,32000)
    # youtubeDL force
    if ytdlp == True:
        if savingFeature:
            if dateFileName == True:
                dateFile = str(currentDate) + "_" + str(randomInt)
            else:
                dateFile = ""
            if not os.path.isdir(saving_path): 
                log.info("Creating directory " + saving_path)
                os.makedirs(saving_path)
            image = saving_path + filename + dateFile + exten
        else:
            image = default_path +  exten
        ytdl = youtube_dl.YoutubeDL({'outtmpl': '' + str(image),})
        with ytdl:
            result = ytdl.download([url])
        return image
    # Get the file extension 
    if exten == "":
        exten = os.path.splitext(url)[1]
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            if resp.status == 200:
                if savingFeature:
                    if dateFileName == True:
                        dateFile = str(currentDate) + "_" + str(randomInt)
                    else:
                        dateFile = ""
                    if not os.path.isdir(saving_path): 
                        log.info("Creating directory " + saving_path)
                        os.makedirs(saving_path)
                    image = saving_path + filename + dateFile + str(exten)
                else:
                    image = default_path + str(exten)
                f = await aiofiles.open(image, mode='wb')
                await f.write(await resp.read())
                await f.close()
                return image
            else:
                log.info("Error downloading stuff.")