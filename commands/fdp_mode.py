# Import of needed libraries
import random
import re
import logging as log

# Import of OBbot specific stuff
import functions.messages as messages
import functions.check as check


async def fdpCall(async_client, event, room_id, config, lang):
    # Retire le texte quand on répond a quelqu'un, pour eviter que le bot pense que c'nous qui disons des choses
    event.body = re.sub('^(>.*\n)+\n', '', event.body, count=0, flags=0)

    # Normal stuff, kinda gentle
    if re.search(r'\b' + "rip" + r'\b', event.body.lower()):
        await messages.sendReact(async_client, room_id, event.event_id, [random.choice(lang.fdp_ripEmote)])
        return
    
    if event.body.lower().startswith(config['prefix'] + "champion") and 'm.relates_to' in event.source["content"]:
        idMsg = event.source["content"]["m.relates_to"]["m.in_reply_to"]["event_id"]
        await messages.sendReact(async_client, room_id, idMsg, ["🇨", "🇭", "🇦", "🇲", "🇵", "🇮", "🇴", "🇳" ])
        return


    if event.sender in config['listAdmin'] and config['can_admin_bypass_fdpmode'] == True:
        return
    if(re.sub(r'[.;,!/:?$^>< ]', '', event.body.lower()).endswith("quoi")):
        await messages.sendMessage(async_client, room_id, random.choice(["feur", "feuse"]))
        return
    if(re.sub(r'[.;,!/:?$^>< ]', '', event.body.lower()).endswith("hein")):
        await messages.sendMessage(async_client, room_id, random.choice(['deux', 'eken']))
        return
    if(re.search('(^(ui|oui|wi){1}([.;,!/:?$^>< ])*$)|([ ](ui|oui|wi){1}([.;,!/:?$^>< ])*$)', event.body.lower()) != None):
        await messages.sendMessage(async_client, room_id, "stiti")
        return
    if(re.sub(r'[.;,!/:?$^>< ]', '', event.body.lower()).endswith("haut")):
        await messages.sendMessage(async_client, room_id, "bas")
        return
    if(event.body.lower().endswith("ez")):
        await messages.sendMessage(async_client, room_id, "pz")
        return
    if(event.body.lower().strip() == ":)"):
        await messages.sendMessage(async_client, room_id, "(:")
        return
    if(re.sub(r'[.;,!/:?$^>< ]', '', event.body.lower()).endswith("toi")):
        await messages.sendMessage(async_client, room_id, "ture")
        return
    if(re.sub(r'[.;,!/:?$^>< ]', '', event.body.lower()).endswith("cheh")):
        await messages.sendMessage(async_client, room_id, random.choice(["ricoco", "rie ❤️", "Guevara 🇨🇺 🇨🇺", "rlock 🧐", "d'oeuvre", "kels"]))
        return
    if(re.search('(((qu)|(k)))((([éèë]|ais|ait|ez)+|((ez)+(z)*))+)$', re.sub(r'[.;,!/:?$^>< ]', '', event.body.lower())) != None):
        await messages.sendMessage(async_client, room_id, "quette")
        return

    if await check.checkMention(event) == config['username']:
        await messages.sendMessage(async_client, room_id, random.choice(lang.fdp_phrasesMentions).replace('event.sender', event.sender))
        return

    if re.search(r'\b' + "cuisine" + r'\b', event.body.lower()):
        await messages.sendMessage(async_client, room_id, "COUISINE!!! https://www.youtube.com/watch?v=adCyaYx4h_M")
        return

    if re.search(r'\b' + "comme meme" + r'\b', event.body.lower()):
        await messages.sendMessage(async_client, room_id, '"comme meme", t\'es vraiment un fils de pute')
        return
        
    if re.search(r'\b' + "apoday" + r'\b', event.body.lower()):
        await messages.sendMessage(async_client, room_id, "Apoday, Best jeu, allez le DL les gars")
        return

    if re.search(r'\b' + "bot" + r'\b', event.body.lower()):
        await messages.sendMessage(async_client, room_id, "Vous parlez de moi les gars ? 👀")
        return

    if re.search(r'\b' + "bug" + r'\b', event.body.lower()):
        await messages.sendMessage(async_client, room_id, "Ouais, j'suis pleins de bugs, dites à @lanseria:matrix.org de se magner le cul et les corriger...")
        return

    if (re.search('[ ]*[@]everyone[ ]*', event.body.lower()) or re.search('[ ]*[@]here[ ]*', event.body.lower())):
        await messages.sendMessage(async_client, room_id, "Écoute moi bien " + event.sender + ", on est pas sur discord enculé de fils de ta race la grosse chienne, refais ca j'tronconne ton arbre généalogique enfoiré. ")
        return


    if "m.new_content" in str(event.source['content']) and event.source['content']['m.new_content'] != "":
        await messages.sendMessage(async_client, room_id, random.choice(lang.fdp_edited).replace('event.sender', event.sender))
        return


    for insult in lang.fdp_insults:
        if re.search(r'\b' + insult + r'\b', event.body.lower()):
            await messages.sendMessage(async_client, room_id, event.sender + " " + random.choice(lang.fdp_phrases))
            break
            return

    for excuse in lang.fdp_excuses:
        if re.search(r'\b' + excuse + r'\b', event.body.lower()):
            await messages.sendMessage(async_client, room_id, "Ouais j'préfère " + event.sender)
            break
            return