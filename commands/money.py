# Import of needed libraries
import time
import random
import aiosqlite

# Import of OBbot specific stuff
import functions.messages as messages
import functions.bases as bases
import functions.check as check


# Functions to show the top players in terms of MONEY
async def showMoneyTop(async_client, room_id, event, database, currencyName):
    await messages.sendTyping(async_client, room_id, True)
    connexion = await aiosqlite.connect(database)
    t = (event.sender,)
    # The database file
    connexion = await aiosqlite.connect(database)
    cursor = await connexion.execute('SELECT money, user, ROW_NUMBER() OVER ( ORDER BY money DESC ) as rank FROM money')
    rank = await cursor.fetchall()
    message = ""
    for arr in rank:
        message = message + "Rank #" + str(arr[2]) + " - " + str(arr[1]) + " / " + str(round(int(arr[0]))) + " " + currencyName + "\n"
    await messages.sendMessage(async_client, room_id, message)
    await connexion.close()
    await messages.sendTyping(async_client, room_id, False)
    return


# Function to check how many money a user have
async def showMoneyOfUser(async_client, room_id, event, database, currencyName, lang):
    userRequested = await check.checkMention(event)
    connexion = await aiosqlite.connect(database)
    if userRequested != "":
        isUserExist = await check.checkUserHaveBank(userRequested, database)
        if isUserExist:
            cursor = await connexion.execute('SELECT money FROM money WHERE user=?;', (userRequested,))
            oldVal = await cursor.fetchone()
            await messages.sendMessage(async_client, room_id, "", lang.currencyUserMoney.replace('lang.option1', userRequested).replace('lang.option2', str(oldVal[0])).replace('lang.option3', currencyName))
        else:
            await messages.sendMessage(async_client, room_id, "", lang.currencyUserMoney.replace('lang.option1', userRequested).replace('lang.option2', "0").replace('lang.option3', currencyName))
    else:
        isUserExist = await check.checkUserHaveBank(event.sender, database)
        if isUserExist:
            cursor = await connexion.execute('SELECT money FROM money WHERE user=?;', (event.sender,))
            oldVal = await cursor.fetchone()
            await messages.sendMessage(async_client, room_id, "", lang.currencyMoneyOwned.replace('lang.option2', str(oldVal[0])).replace('lang.option3', currencyName).replace('lang.option1', event.sender))
        else:
            await messages.sendMessage(async_client, room_id, "", lang.currencyMoneyOwned.replace('lang.option2', "0").replace('lang.option3', currencyName).replace('lang.option1', event.sender))
    await connexion.commit()
    await connexion.close()


# Function to give money from an user to another user
async def giveMoneyToUser(async_client, room_id, event, database, currencyName, lang, options, prefix):
    if await check.checkMention(event) != event.sender:
        # Si il y a 3 argument et que la money est in int et supérieur à 1
        if len(options) == 3 and options[2].isnumeric() and int(options[2]) > 0:
            isUserExist = await check.checkUserHaveBank(event.sender, database)
            # Si l'user qui donne est dans la BDD, si c'est non, il n'a pas assez d'argent
            if isUserExist:
                connexion = await aiosqlite.connect(database)
                cursor = await connexion.execute('SELECT money FROM money WHERE user=?;', (event.sender,))
                oldVal = await cursor.fetchone()
                # Si l'user qui donne a assez d'argent-
                if(oldVal[0] >= int(options[2])):
                    userRequested = await check.checkMention(event)
                    # Si on a donné un utilisateur valide
                    if userRequested != "":
                        isUserExist = await check.checkUserHaveBank(userRequested, database)
                        valuesSend = (oldVal[0]-int(options[2]), event.sender,)
                        # Si Le mec qui recois est dans la BDD ou non
                        if isUserExist:
                            cursor = await connexion.execute('SELECT money FROM money WHERE user=?;', (userRequested,))
                            oldValRec = await cursor.fetchone()
                            valuesReceive = (oldValRec[0]+int(options[2]), userRequested,)
                            await connexion.execute('UPDATE money SET money=? WHERE user=?', valuesSend)
                            await connexion.execute('UPDATE money SET money=? WHERE user=?', valuesReceive)
                            await messages.sendMessage(async_client, room_id, "", lang.currencyTradeGaveMoney.replace('lang.option1', event.sender).replace('lang.option2', options[2]).replace('lang.option3', currencyName).replace('lang.option4', userRequested))
                        else:
                            valuesReceive = (userRequested, int(options[2]),)
                            await connexion.execute('UPDATE money SET money=? WHERE user=?', valuesSend)
                            await connexion.execute('INSERT INTO money (user, money) VALUES (?, ?)', valuesReceive)
                            await messages.sendMessage(async_client, room_id, "", lang.currencyTradeGaveMoney.replace('lang.option1', event.sender).replace('lang.option2', options[2]).replace('lang.option3', currencyName).replace('lang.option4', userRequested))
                    else:
                        await messages.sendMessage(async_client, room_id, "", lang.currencyTradeSyntax.replace('lang.option1', event.sender).replace('lang.option2', prefix))
                else:
                    await messages.sendMessage(async_client, room_id, "", lang.currencyTradeNotEnoughMoney.replace('lang.option1', event.sender))
                await connexion.commit()
                await connexion.close()
            else:
                await messages.sendMessage(async_client, room_id, "", lang.currencyTradeNotEnoughMoney.replace('lang.option1', event.sender))
        else:
            await messages.sendMessage(async_client, room_id, "", lang.currencyTradeSyntax.replace('lang.option1', event.sender).replace('lang.option2', prefix))
    else:
        await messages.sendMessage(async_client, room_id, "", lang.currencyTradeCantGiveToSelf.replace('lang.option1', event.sender))



# Functions to give bonus money to users
async def giveBonusMoney(async_client, room_id, event, timeType, lang, database, currencyName):
    if timeType == "monthly":
        columnname = "latestmonthly"
        timeToSubsctact = 2592000
        money_num = random.randrange(300,1000)
        givenMessage = lang.currencyMonthlyGiven
        alreadyClamedMessage = lang.currencyMonthlyAlreadyClaimed
    if timeType == "weekly":
        columnname = "latestweekly"
        timeToSubsctact = 604800
        money_num = random.randrange(50,100)
        givenMessage = lang.currencyWeeklyGiven
        alreadyClamedMessage = lang.currencyWeeklyAlreadyClaimed
    if timeType == "daily":
        columnname = "latestdaily"
        timeToSubsctact = 86400
        money_num = random.randrange(0,25)
        givenMessage = lang.currencyDailyGiven
        alreadyClamedMessage = lang.currencyDailyAlreadyClaimed

    timeAsked = round(time.time())
    isUserExist = await check.checkUserHaveBank(event.sender, database)
    connexion = await aiosqlite.connect(database)
    if isUserExist:
        cursor = await connexion.execute('SELECT money, ' + columnname + ' FROM money WHERE user=?;', (event.sender,))
        userVal = await cursor.fetchone()
        if(userVal[1] <= round(time.time()-timeToSubsctact)):
            newMoney = userVal[0] + money_num
            values = (newMoney, timeAsked, event.sender,)
            await connexion.execute('UPDATE money SET money=?, ' + columnname + '=? WHERE user=?', values)
            await messages.sendMessage(async_client, room_id, "", givenMessage.replace('lang.option3', event.sender).replace('lang.option1', str(money_num)).replace('lang.option2', currencyName))
        else:
            nextAvailable = (round(time.time()) - userVal[1])
            nextAvailable = await bases.ConvertSectoDay(timeToSubsctact - nextAvailable, lang)
            await messages.sendMessage(async_client, room_id, "", alreadyClamedMessage.replace('lang.option1', nextAvailable))
    else:
        # Existe pas
        values = (event.sender, money_num, timeAsked,)
        await connexion.execute('INSERT INTO money (user, money, ' + columnname + ') VALUES (?, ?, ?)', values)
        await messages.sendMessage(async_client, room_id, "", givenMessage.replace('lang.option3', event.sender).replace('lang.option1', str(money_num)).replace('lang.option2', currencyName))
    await connexion.commit()
    await connexion.close()