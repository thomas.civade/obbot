import os
import sys
import json
import traceback
import logging as log

# Async Functions:
import aiofiles
import asyncio

# Matrix/Nio Functions:
from nio import ( AsyncClient, InviteEvent, Event, SyncResponse, RoomMessageText, InviteMemberEvent, AsyncClientConfig,
    LoginResponse, KeyVerificationEvent, KeyVerificationStart, KeyVerificationCancel, KeyVerificationKey, KeyVerificationMac, 
    ToDeviceError, LocalProtocolError, crypto, store, exceptions, MatrixRoom, JoinError, MegolmEvent )

# OBbot Functions:
import functions.bases as bases
import functions.check as check
import functions.messages as messages
import functions.download as download
import commands.xp as xp
import commands.money as money
import commands.anime as anime
import commands.admin as admin
import commands.nsfw as nsfw_cmd
import commands.normal as normal
import commands.reddit as reddit
import commands.lemmy as lemmy
import commands.fdp_mode as fdp_mode
import commands.settings as settings
import commands.truthordare as truthordare

# The Config File
with open("config.json", "r") as f:
    configBotLogin = json.loads(f.read())

config = configBotLogin
# Prefix
prefix = config["prefix"]
# The FDP mode
if config["fdpmode"] == "1":
    fdpMode = True
else: 
    fdpMode = False
# Saving features
if config["savingfeature"] == "1":
    savingFeature = True
else: 
    savingFeature = False

# La langue
if config['lang'] == "fr":
    import lang.fr as lang
elif config['lang'] == "en":
    import lang.en as lang


logger = log.getLogger()
logger.setLevel(log.INFO)
formatter = log.Formatter('%(asctime)s | %(levelname)s | %(message)s', '%d-%m-%Y %H:%M:%S')
# Log inside the terminal
stdout_handler = log.StreamHandler(sys.stdout)
stdout_handler.setFormatter(formatter)
# Log inside a file
file_handler = log.FileHandler(configBotLogin['log_file'])
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stdout_handler)

# class Config:
#     def __init__(self):
        

class Callbacks:
    def __init__(self, async_client: AsyncClient, config):
        """
        Args:
            client: nio client used to interact with matrix.
            config: Bot configuration parameters.
        """
        self.async_client = async_client
        self.config = config

    async def commandsCall(self, room: MatrixRoom, event: RoomMessageText):
        """
        Args:
            room: Room where the event was sent.
            event: Event to manage, here, a message.
        """
        # The Config File
        with open("config.json", "r") as f:
            config = json.loads(f.read())

        # Prefix
        prefix = config["prefix"]
        # The FDP mode
        if config["fdpmode"] == "1":
            fdpMode = True
        else: 
            fdpMode = False
        # Saving features
        if config["savingfeature"] == "1":
            savingFeature = True
        else: 
            savingFeature = False

        # La langue
        if config['lang'] == "fr":
            import lang.fr as lang
        elif config['lang'] == "en":
            import lang.en as lang

        room_id = room.room_id
        async_client = self.async_client

        if "body" in event.source['content'].keys() and event.sender != config['username']:
            await xp.xpMessageCall(async_client, event, room_id, config, lang)
            log.info(event.source)
            if event.body.startswith(prefix) and event.body != "!":
                # Replace multiple spaces with only one in commands:
                event.body = ' '.join(event.body.split())
                if event.body.lower() == prefix + "board":
                    await normal.sendXPLeaderBoard(async_client, room_id, event, lang, config['database'])

                if event.body.lower().startswith(prefix + "level"):
                    await normal.sendProfileCard(async_client, room_id, event, lang, config['database'], config)

                # Pour changer la couleur!
                if event.body.lower().startswith(prefix + "profile color"):
                    await normal.changeProfileColor(async_client, room_id, event, lang, config['database'])

                ###################################
                ##        THE NORMAL ZONE        ##
                ###################################
                if event.body.lower().startswith(prefix + "help"):
                    helpOptions = event.body.lower().replace(prefix + "help", "", 1).strip()
                    await messages.sendMessage(async_client, room_id, "", lang.helpFonction(prefix, helpOptions)
                        .replace('lang.userlemmynsfw', ', '.join(config['listAllowedUserLemmyNSFW']))
                        .replace('lang.userlemmysfw', ', '.join(config['listAllowedUserLemmySFW']))
                        .replace('lang.communitieslemmynsfw', ', '.join(config['listAllowedLemmyCommunitiesNSFW']))
                        .replace('lang.communitieslemmysfw', ', '.join(config['listAllowedLemmyCommunitiesSFW']))
                        .replace('lang.userredditnsfw', ', '.join(config['listAllowedUserRedditNSFW']))
                        .replace('lang.userredditsfw', ', '.join(config['listAllowedUserRedditSFW']))
                        .replace('lang.subredditnsfw', ', '.join(config['listAllowedSubredditNSFW']))
                        .replace('lang.subredditsfw', ', '.join(config['listAllowedSubredditSFW']))
                        .replace('lang.animesearchnsfw', ', '.join(config['listAnimeSearchOptionsNSFW']))
                        .replace('lang.animesearchsfw', ', '.join(config['listAnimeSearchOptionsSFW']))
                        .replace('lang.emotes', ', '.join(config['listFileNameEmote'])))

                if event.body.lower() == prefix + "credits":
                    await messages.sendMessage(async_client, room_id, lang.creditsMessage)

                if event.body.lower() == prefix + "ping":
                    time = await check.checkMessagePing(event)
                    await messages.sendMessage(async_client, room_id, "Pong! (" + str(time) + " ms)")

                if event.body.lower().startswith(prefix + "emote"):
                    emoteFile = event.body.lower().replace(prefix + "emote", "", 1).strip()
                    if os.path.isfile("./emotes/" + emoteFile + ".png"):
                        emote = './emotes/' + emoteFile + ".png"
                        await messages.sendImage(async_client, room_id, emote)

                if event.body.startswith(prefix + "sauce"):
                    await normal.getSauce(async_client, room_id, event, lang, config)

                if event.body.startswith(prefix + "html"):
                    htmlVal = event.body.replace(prefix + "html", "", 1).strip()
                    await messages.sendMessage(async_client, room_id, htmlVal, htmlVal)

                if event.body.lower().startswith(prefix + "infos"):
                    await normal.userInfos(async_client, room_id, event, lang, config)

                ###################################
                ##     THE TRUTH OR DARE ZONE    ##
                ###################################
                if event.body.startswith(prefix + "truth"):
                    await truthordare.sendTruth(async_client, room_id, event, config['database'], lang)

                if event.body.startswith(prefix + "dare"):
                    await truthordare.sendDare(async_client, room_id, event, config['database'], lang)

                if event.body.lower().startswith(prefix + "profile tod"):
                    settingToD = event.body.lower().replace(prefix + "profile tod", "", 1).strip()
                    await truthordare.userSettingToD(async_client, room_id, event, config['database'], settingToD, lang)

                ###################################
                ##         THE MONEY ZONE        ##
                ###################################
                if event.body.startswith(prefix + "money") or event.body.startswith(prefix  + "baltop"):
                    options = event.body.lower().replace(prefix + "money", "", 1).strip().split()
                    if(len(options) > 0 and options[0] == "top") or event.body.startswith(prefix  + "baltop"):
                        await money.showMoneyTop(async_client, room_id, event, config['database'], config['currencyName'])
                        return
                    elif(len(options) > 0 and options[0] == "give"):
                        await money.giveMoneyToUser(async_client, room_id, event, config['database'], config['currencyName'], lang, options, prefix)
                        return
                    else:
                        await money.showMoneyOfUser(async_client, room_id, event, config['database'], config['currencyName'], lang)

                # Commands to give bonus Money
                if event.body.startswith(prefix + "daily"):
                    await money.giveBonusMoney(async_client, room_id, event, "daily", lang, config['database'], config['currencyName'])

                if event.body.startswith(prefix + "weekly"):
                    await money.giveBonusMoney(async_client, room_id, event, "weekly", lang, config['database'], config['currencyName'])

                if event.body.startswith(prefix + "monthly"):
                    await money.giveBonusMoney(async_client, room_id, event, "monthly", lang, config['database'], config['currencyName'])

                ###################################
                ##         THE NSFW ZONE         ##
                ###################################
                if (event.body.lower().split()[0] in [prefix + "an", prefix + "anime"]
                or event.body.lower().replace(prefix, "", 1).split()[0] in config['animeAliasesNSFW'] + config['animeAliasesSFW']):
                    await anime.searchAnime(async_client, room_id, event, lang, config, prefix, savingFeature)

                # if (event.body.lower().split()[0] in [prefix + "r", prefix + "reddit"]
                # or event.body.lower().replace(prefix, "", 1).split()[0] in config['subredditAliasesNSFW'] + config['subredditAliasesSFW']):
                #     await reddit.searchReddit(async_client, room_id, event, lang, config, prefix, savingFeature)

                if (event.body.lower().split()[0] in [prefix + "r", prefix + "reddit"]):
                    await reddit.searchReddit(async_client, room_id, event, lang, config, prefix, savingFeature)

                if (event.body.lower().split()[0] in [prefix + "l", prefix + "lemmy"]
                or event.body.lower().replace(prefix, "", 1).split()[0] in config['AliasesNSFW'] + config['AliasesSFW']):
                    await lemmy.searchLemmy(async_client, room_id, event, lang, config, prefix, savingFeature)

                if event.body.lower().startswith(prefix + "rule34"):
                    await nsfw_cmd.sendR34Image(async_client, room_id, event, config, prefix, savingFeature)

                if event.body.lower().startswith(prefix + "pornhub"):
                    await nsfw_cmd.sendPornHubLink(async_client, room_id, event, config, prefix, savingFeature)

                if event.body.lower() == prefix + "random":
                    await nsfw_cmd.sendRandomFileFromLibrary(async_client, room_id, event, config)

                ###################################
                ##        THE ADMIN ZONE         ##
                ###################################
                if await bases.isAdmin(event.sender, config) or await bases.isMod(event.sender, config) :
                    # If you want to kick a user via chat (why not)
                    if event.body.lower().startswith(prefix + "kick"):
                        if bases.hasPermission(event.sender, config, "canKickUsers"):
                            await admin.kickUser(async_client, room_id, event, lang)
                        else:
                            await messages.sendMessage(async_client, room_id, "", lang.notAllowedToDoThat.replace("lang.option1", event.sender))

                    # Clear last n Messages
                    if event.body.lower().startswith(prefix + "clear"):
                        if bases.hasPermission(event.sender, config, "canClearMessages"):
                            await admin.clearMessage(async_client, room_id, event, lang, prefix)
                        else:
                            await messages.sendMessage(async_client, room_id, "", lang.notAllowedToDoThat.replace("lang.option1", event.sender))

                    # Settings commands
                    if event.body.lower().startswith(prefix + "setting"):
                        await settings.settingChangeHandler(async_client, room_id, event, prefix, lang, config)

            elif fdpMode:
                await fdp_mode.fdpCall(async_client, event, room_id, config, lang)
            else:
                log.info("Not a message for me")

    async def invite(self, room: MatrixRoom, event: InviteMemberEvent) -> None:
        """Callback for when an invite is received. Join the room specified in the invite.

        Args:
            room: The room that we are invited to.

            event: The invite event.
        """
        logger.debug(f"Got invite to {room.room_id} from {event.sender}.")

        # Attempt to join 3 times before giving up
        for attempt in range(3):
            result = await self.async_client.join(room.room_id)
            if type(result) == JoinError:
                logger.error(
                    f"Error joining room {room.room_id} (attempt %d): %s",
                    attempt,
                    result.message,
                )
            else:
                break
        else:
            logger.error("Unable to join room: %s", room.room_id)

        # Successfully joined room
        logger.info(f"Joined {room.room_id}")

    async def invite_event_filtered_callback(self, room: MatrixRoom, event: InviteMemberEvent) -> None:
        """
        Since the InviteMemberEvent is fired for every m.room.member state received
        in a sync response's `rooms.invite` section, we will receive some that are
        not actually our own invite event (such as the inviter's membership).
        This makes sure we only call `callbacks.invite` with our own invite events.
        """
        if event.state_key == self.async_client.user_id:
            # This is our own membership (invite) event
            await self.invite(room, event)


async def main() -> None:
    # The Matrix client
    async_client_config = AsyncClientConfig(max_limit_exceeded=0, max_timeouts=0, store_sync_tokens=True, encryption_enabled=True)
    async_client = AsyncClient(configBotLogin['homeserver'], configBotLogin['username'], device_id=configBotLogin['device_id'], store_path=configBotLogin['storage_path'], config=async_client_config)

    if configBotLogin['password'] != "":
        # If we use a password, we use it to generate an access token for future logins.
        response = await async_client.login(configBotLogin['password'], configBotLogin['device_name'])
        # Si une erreur surviens lors du login
        if not isinstance(response, LoginResponse):
            print(f"Failed to log in (bad password ?): {response}")
            sys.exit(1)
        log.info("Password used. Changing config file...")
        await settings.setSetting("password", "")
        await settings.setSetting("access_token", str(response.access_token))
        await settings.setSetting("device_id", str(response.device_id))
        log.info("Login successful. (Using new credentials, config file updated)")
    else:
        # Or else just use the access token:
        response = async_client.restore_login(user_id=configBotLogin['username'], device_id=configBotLogin['device_id'], access_token=configBotLogin['access_token'])
        log.info("Login successful. (Using old credentials)")

    if async_client.should_upload_keys:
        await async_client.keys_upload()

    callbacks = Callbacks(async_client, configBotLogin)
    async_client.add_event_callback(callbacks.commandsCall, RoomMessageText)
    async_client.add_event_callback(callbacks.invite_event_filtered_callback, (InviteMemberEvent,))

    await async_client.sync_forever(timeout=30000)  # milliseconds


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        os._exit(0)
    except Exception:
        log.info(traceback.format_exc())
        os._exit(0)