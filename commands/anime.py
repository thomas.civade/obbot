# Import of needed libraries
import os
import json
import random
import logging as log
from urllib.request import urlopen

# Import of OBbot specific stuff
import functions.bases as bases
import functions.download as download
import functions.messages as messages


async def searchAnime(async_client, room_id, event, lang, config, prefix, savingFeature):
    # Get the search terms
    if(event.body.lower().split()[0] in [prefix + "an", prefix + "anime"]):
        search = event.body.lower().replace(prefix + "anime", "", 1).strip().replace(prefix + "an", "", 1).strip()
    else:
        search = event.body.lower().replace(prefix, "", 1).split()[0]

    # Si le channel n'est pas dans la NSFW room list
    if (search in config['listAnimeSearchOptionsNSFW'] + config['listAnimeSearchOptionsSFW']
    and room_id not in config['nsfw_rooms']):
        await messages.sendMessage(async_client, room_id, lang.NSFWDisable, "")
        return

    # Si le search n'est pas dans les listes autorisées
    if (search not in (config['listAnimeSearchOptionsNSFW'] + config['listAnimeSearchOptionsSFW'] + config['animeAliasesSFW'] + config['animeAliasesNSFW'])):
        await messages.sendMessage(async_client, room_id, await bases.removeHTML(lang.AnimeNotInList.replace('lang.option1', prefix)), lang.AnimeNotInList.replace('lang.option1', prefix))
        return

    await messages.sendTyping(async_client, room_id, True)
    urlNekos = "http://api.nekos.fun:8080/api/" + search
    response = urlopen(urlNekos)
    data_json = json.loads(response.read())
    imageURL = data_json['image']

    image = await  download.downloadFile(savingFeature, imageURL, './image', './library/anime/' + str(search) +'/', 'NekosFun_')
    await messages.sendImage(async_client, room_id, image)
    await messages.sendTyping(async_client, room_id, False)
    await bases.saveSauce(room_id, imageURL, "!anime " + search, config['storage_path'])

