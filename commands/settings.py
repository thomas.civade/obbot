# Import of needed libraries
import json
import aiofiles
import logging as log

# Import of OBbot specific stuff
import functions.messages as messages
import functions.bases as bases

# Function to change the settings file
async def setSetting(setting_name, value_set = 0, removeFromArray = False, isArray = False):
    # The config file
    async with aiofiles.open("config.json", "r") as f:
        newConfig = json.loads(await f.read())
    if isArray and not removeFromArray:
        newConfig[setting_name] = newConfig[setting_name] + [value_set]
    elif isArray and removeFromArray:
        for (room_list) in newConfig[setting_name]:
            if str(value_set) in room_list:
                newConfig[setting_name].remove(value_set)
    else:
        newConfig[setting_name] = value_set
    out_file = open("config.json", "w")
    json.dump(newConfig, out_file, indent = 4, sort_keys = False)
    log.info("Paramètre changé : " + setting_name + " => " + value_set)


# Function to handle the change of any setting
async def settingChangeHandler(async_client, room_id, event, prefix, lang, config):
    settingToChange = event.body.lower().replace(prefix + "setting", "", 1).strip().split(" ")
    # Set le prefix
    if settingToChange[0] == "prefix":
        if await bases.hasPermission(event.sender, config, "canChangePrefix"):
            prefix_updated = settingToChange[1]
            await setSetting("prefix", prefix_updated)
            await messages.sendMessage(async_client, room_id, "", lang.setPrefixMessage.replace("lang.option1", prefix_updated))
        else:
            await messages.sendMessage(async_client, room_id, "", lang.notAllowedToDoThat.replace("lang.option1", event.sender))

    # Le FDP mode
    if settingToChange[0] == "fdpmode":
        if await bases.hasPermission(event.sender, config, "canChangeFDPModeStatus"):
            fdpmode_updated = settingToChange[1]
            if fdpmode_updated == "enable":
                status = ["1", lang.enableMessage]
            elif fdpmode_updated == "disable":
                status = ["0", lang.disableMessage]
            else: 
                await messages.sendMessage(async_client, room_id, "", lang.NoGoodOptionMessage.replace("lang.option1", "enable, disable"))
                return
            await setSetting("fdpmode", status[0])
            await messages.sendMessage(async_client, room_id, lang.setFDPMessage.replace("lang.option1", status[1]))
        else:
            await messages.sendMessage(async_client, room_id, "", lang.notAllowedToDoThat.replace("lang.option1", event.sender))

    # Le NSFW mode
    if settingToChange[0] == "nsfw":
        if await bases.hasPermission(event.sender, config, "canChangeNSFWRoomStatus"):
            nsfwRoom_updated = settingToChange[1]
            if nsfwRoom_updated == "enable":
                if room_id in config['nsfw_rooms']:
                    await messages.sendMessage(async_client, room_id, lang.alreadyNSFWRoom)
                    return
                status = [room_id, lang.enableMessage, False]
            elif nsfwRoom_updated == "disable":
                status = [room_id, lang.disableMessage, True]
            else: 
                await messages.sendMessage(async_client, room_id, "",  lang.NoGoodOptionMessage.replace("lang.option1", "enable, disable"))
                return
            await setSetting("nsfw_rooms", status[0], status[2], True)
            await messages.sendMessage(async_client, room_id, lang.setNSFWMessage.replace("lang.option1", status[1]))# Le NSFW mode
        else:
            await messages.sendMessage(async_client, room_id, "", lang.notAllowedToDoThat.replace("lang.option1", event.sender))


    if settingToChange[0] == "reddit":
        if await bases.hasPermission(event.sender, config, "canEditRedditAllowedLists"):
            if len(settingToChange) < 4:
                await messages.sendMessage(async_client, room_id, "", lang.NotEnoughOptionMessage.replace("lang.option1", prefix + "help admin"))
                return
            typesubreddit = settingToChange[1]
            actionsubreddit = settingToChange[2]
            subreddittochange = settingToChange[3]
            if(typesubreddit == "nsfw"):
                arrayToChange = "listAllowedSubredditNSFW"
                messageAdded = lang.subredditAddedToTheList
                messageRemoved = lang.subredditRemovedToTheList
                messageAlreadyOnList = lang.subredditAlreadyOnTheList
            elif(typesubreddit == "sfw"):
                arrayToChange = "listAllowedSubredditSFW"
                messageAdded = lang.subredditAddedToTheList
                messageRemoved = lang.subredditRemovedToTheList
                messageAlreadyOnList = lang.subredditAlreadyOnTheList
            elif(typesubreddit == "user_nsfw"):
                arrayToChange = "listAllowedUserRedditNSFW"
                messageAdded = lang.redditUserAddedToTheList
                messageRemoved = lang.redditUserRemovedToTheList
                messageAlreadyOnList = lang.redditUserAlreadyOnTheList
            elif(typesubreddit == "user_sfw"):
                arrayToChange = "listAllowedUserRedditSFW"
                messageAdded = lang.redditUserAddedToTheList
                messageRemoved = lang.redditUserRemovedToTheList
                messageAlreadyOnList = lang.redditUserAlreadyOnTheList
            else:
                await messages.sendMessage(async_client, room_id, "", lang.NoGoodOptionMessage.replace("lang.option1", "nsfw, sfw, user_nsfw, user_sfw"))
                return
            
            if actionsubreddit == "enable":
                if subreddittochange in config[arrayToChange]:
                    await messages.sendMessage(async_client, room_id, messageAlreadyOnList)
                    return
                status = [subreddittochange, messageAdded, False]
            elif actionsubreddit == "disable":
                status = [subreddittochange, messageRemoved, True]
            else: 
                await messages.sendMessage(async_client, room_id, "", lang.NoGoodOptionMessage.replace("lang.option1", "enable, disable"))
                return
            await setSetting(arrayToChange, status[0], status[2], True)
            await messages.sendMessage(async_client, room_id, status[1])
        else:
            await messages.sendMessage(async_client, room_id, "", lang.notAllowedToDoThat.replace("lang.option1", event.sender))


    # Adding/Removing lemmy stuff to allowed lists
    if settingToChange[0] == "lemmy":
        if await bases.hasPermission(event.sender, config, "canEditRedditAllowedLists"):
            if len(settingToChange) < 4:
                await messages.sendMessage(async_client, room_id, "", lang.NotEnoughOptionMessage.replace("lang.option1", prefix + "help admin"))
                return
            typesubreddit = settingToChange[1]
            actionsubreddit = settingToChange[2]
            subreddittochange = settingToChange[3]
            if(typesubreddit == "nsfw"):
                arrayToChange = "listAllowedLemmyCommunitiesNSFW"
                messageAdded = lang.LemmyCommunityAddedToTheList
                messageRemoved = lang.LemmyCommunityRemovedToTheList
                messageAlreadyOnList = lang.LemmyCommunityAlreadyOnTheList
            elif(typesubreddit == "sfw"):
                arrayToChange = "listAllowedLemmyCommunitiesSFW"
                messageAdded = lang.LemmyCommunityAddedToTheList
                messageRemoved = lang.LemmyCommunityRemovedToTheList
                messageAlreadyOnList = lang.LemmyCommunityAlreadyOnTheList
            elif(typesubreddit == "user_nsfw"):
                arrayToChange = "listAllowedUserLemmyNSFW"
                messageAdded = lang.LemmyUserAddedToTheList
                messageRemoved = lang.LemmyUserRemovedToTheList
                messageAlreadyOnList = lang.LemmyUserAlreadyOnTheList
            elif(typesubreddit == "user_sfw"):
                arrayToChange = "listAllowedUserLemmySFW"
                messageAdded = lang.LemmyUserAddedToTheList
                messageRemoved = lang.LemmyUserRemovedToTheList
                messageAlreadyOnList = lang.LemmyUserAlreadyOnTheList
            else:
                await messages.sendMessage(async_client, room_id, "", lang.NoGoodOptionMessage.replace("lang.option1", "nsfw, sfw, user_nsfw, user_sfw"))
                return
            
            if actionsubreddit == "enable":
                if subreddittochange in config[arrayToChange]:
                    await messages.sendMessage(async_client, room_id, messageAlreadyOnList)
                    return
                status = [subreddittochange, messageAdded, False]
            elif actionsubreddit == "disable":
                status = [subreddittochange, messageRemoved, True]
            else: 
                await messages.sendMessage(async_client, room_id, "", lang.NoGoodOptionMessage.replace("lang.option1", "enable, disable"))
                return
            await setSetting(arrayToChange, status[0], status[2], True)
            await messages.sendMessage(async_client, room_id, status[1])
        else:
            await messages.sendMessage(async_client, room_id, "", lang.notAllowedToDoThat.replace("lang.option1", event.sender))


    # Saving Feature
    if settingToChange[0] == "save":
        if await bases.hasPermission(event.sender, config, "canChangeSavingFeature"):
            saving_updated = settingToChange[1]
            if saving_updated == "enable":
                status = ["1", lang.enableMessage]
            elif saving_updated == "disable":
                status = ["0", lang.disableMessage]
            else: 
                await messages.sendMessage(async_client, room_id, "", lang.NoGoodOptionMessage.replace("lang.option1", "enable, disable"))
                return
            await setSetting("savingfeature", status[0])
            await messages.sendMessage(async_client, room_id, lang.setSavingMessage.replace("lang.option1", status[1]))
        else:
            await messages.sendMessage(async_client, room_id, "", lang.notAllowedToDoThat.replace("lang.option1", event.sender))

    # Changer la langue
    if settingToChange[0] == "lang":
        if await bases.hasPermission(event.sender, config, "canChangeLanguage"):
            lang_updated = settingToChange[1]
            if lang_updated == "en":
                status = ["en", "english"]
            elif lang_updated == "fr":
                status = ["fr", "français"]
            else: 
                return
            await setSetting("lang", status[0])
            await messages.sendMessage(async_client, room_id, lang.setLangMessage.replace("lang.option1", status[1]))
        else:
            await messages.sendMessage(async_client, room_id, "", lang.notAllowedToDoThat.replace("lang.option1", event.sender))
