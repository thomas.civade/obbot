from nio import (
    AsyncClient,
    AsyncClientConfig,
    LoginResponse,
    KeyVerificationEvent,
    KeyVerificationStart,
    KeyVerificationCancel,
    KeyVerificationKey,
    KeyVerificationMac,
    ToDeviceError,
    LocalProtocolError,
)
import traceback
import getpass
import sys
import os
import json
import asyncio

CONFIG_FILE = "config.json"  # login credentials JSON file
STORE_PATH = "./bot_storage/"  # local directory

class Callbacks(object):
    def __init__(self, client):
        self.client = client

    async def to_device_callback(self, event):
        try:
            client = self.client

            if isinstance(event, KeyVerificationStart): 
                if "emoji" not in event.short_authentication_string:
                    print("Other device does not support emoji verification "
                          f"{event.short_authentication_string}.")
                    return
                resp = await client.accept_key_verification(
                    event.transaction_id)
                if isinstance(resp, ToDeviceError):
                    print(f"accept_key_verification failed with {resp}")

                sas = client.key_verifications[event.transaction_id]

                todevice_msg = sas.share_key()
                resp = await client.to_device(todevice_msg)
                if isinstance(resp, ToDeviceError):
                    print(f"to_device failed with {resp}")

            elif isinstance(event, KeyVerificationCancel):  # anytime
                print(f"Verification has been cancelled by {event.sender} "
                      f"for reason \"{event.reason}\".")

            elif isinstance(event, KeyVerificationKey):  # second step
                sas = client.key_verifications[event.transaction_id]

                print(f"{sas.get_emoji()}")

                yn = input("Do the emojis match? (Y/N) (C for Cancel) ")
                if yn.lower() == "y":
                    print("Match! The verification for this device will be accepted.")
                    resp = await client.confirm_short_auth_string(
                        event.transaction_id)
                    if isinstance(resp, ToDeviceError):
                        print(f"confirm_short_auth_string failed with {resp}")
                elif yn.lower() == "n":  # no, don't match, reject
                    print("No match! Device will NOT be verified "
                          "by rejecting verification.")
                    resp = await client.cancel_key_verification(
                        event.transaction_id, reject=True)
                    if isinstance(resp, ToDeviceError):
                        print(f"cancel_key_verification failed with {resp}")
                else:  # C or anything for cancel
                    print("Cancelled by user! Verification will be cancelled.")
                    resp = await client.cancel_key_verification(
                        event.transaction_id, reject=False)
                    if isinstance(resp, ToDeviceError):
                        print(f"cancel_key_verification failed with {resp}")

            elif isinstance(event, KeyVerificationMac):  # third step
                sas = client.key_verifications[event.transaction_id]
                try:
                    todevice_msg = sas.get_mac()
                except LocalProtocolError as e:
                    # e.g. it might have been cancelled by ourselves
                    print(f"Cancelled or protocol error: Reason: {e}.\n"
                          f"Verification with {event.sender} not concluded. "
                          "Try again?")
                else:
                    resp = await client.to_device(todevice_msg)
                    if isinstance(resp, ToDeviceError):
                        print(f"to_device failed with {resp}")
                    print(f"sas.we_started_it = {sas.we_started_it}\n"
                          f"sas.sas_accepted = {sas.sas_accepted}\n"
                          f"sas.canceled = {sas.canceled}\n"
                          f"sas.timed_out = {sas.timed_out}\n"
                          f"sas.verified = {sas.verified}\n"
                          f"sas.verified_devices = {sas.verified_devices}\n")
                    print("Emoji verification was successful!\n"
                          "Hit Control-C to stop the program or "
                          "initiate another Emoji verification from "
                          "another device or room.")
            else:
                print(f"Received unexpected event type {type(event)}. "
                      f"Event is {event}. Event will be ignored.")
        except BaseException:
            print(traceback.format_exc())

async def login() -> AsyncClient:
    # Configuration options for the AsyncClient
    client_config = AsyncClientConfig(
        max_limit_exceeded=0,
        max_timeouts=0,
        store_sync_tokens=True,
        encryption_enabled=True,
    )
    # open the file in read-only mode
    with open(CONFIG_FILE, "r") as f:
        config = json.load(f)
        # Initialize the matrix client based on credentials from file
        client = AsyncClient(
            config['homeserver'],
            config['username'],
            device_id=config['device_id'],
            store_path=STORE_PATH,
            config=client_config,
        )

        client.restore_login(
            user_id=config['username'],
            device_id=config['device_id'],
            access_token=config['access_token']
        )
    print("Logged in using stored credentials.")
    return client

async def main() -> None:
    client = await login()
    # Set up event callbacks
    callbacks = Callbacks(client)
    client.add_to_device_callback(
        callbacks.to_device_callback, (KeyVerificationEvent,))
    # Sync encryption keys with the server
    # Required for participating in encrypted rooms
    if client.should_upload_keys:
        await client.keys_upload()
    print("This program is ready and waiting for the other party to initiate "
          "an emoji verification with us by selecting \"Verify by Emoji\" "
          "in their Matrix client.")
    await client.sync_forever(timeout=30000, full_state=True)

try:
    asyncio.run(main())
except Exception:
    print(traceback.format_exc())
    sys.exit(1)
except KeyboardInterrupt:
    print("Received keyboard interrupt.")
    sys.exit(0)